# Form Input (0.2.1)


## 0.2 Release 
 - Added a reset form function.
 - Reset the forms
 - forms-input comsumed the forms-input-illustrator element
 - Validation fix

# Supported Types 
1. Varchar
2. Text
3. Number
4. Toggle


# Backloged types
1. User Rate element - needs to be in row with Group Verified User element
2. Group Verified User Element
